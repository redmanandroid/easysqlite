package in.eightfolds.easysqlite.commons.db;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteQueryBuilder;

import java.lang.reflect.InvocationTargetException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import in.eightfolds.easysqlite.commons.db.bean.CommonsEntity;
import in.eightfolds.easysqlite.commons.db.criteria.Criteria;
import in.eightfolds.easysqlite.commons.db.criteria.CriteriaFactory;
import in.eightfolds.easysqlite.commons.db.exception.DaoException;
import in.eightfolds.easysqlite.commons.util.Collections;


public class EntityDaoImpl implements EntityDao {

    private Class<? extends CommonsEntity> clss;
    private Context context;

    public EntityDaoImpl(Context context, Class<? extends CommonsEntity> clss) {
        this.clss = clss;
        this.context = context;
    }

    @Override
    public long add(CommonsEntity entity) throws DaoException,
            IllegalAccessException, IllegalArgumentException {
        return DBUtil.getInstance(context).add(context, entity, clss);
    }

    @Override
    public void add(List<? extends CommonsEntity> entities)
            throws DaoException, IllegalAccessException,
            IllegalArgumentException {
        if (!Collections.isListEmpty(entities)) {
            for (CommonsEntity entity : entities) {
                DBUtil.getInstance(context).add(context, entity, clss);
            }
        }
    }

    @Override
    public int modify(CommonsEntity entity) throws DaoException,
            IllegalAccessException, InvocationTargetException,
            NoSuchMethodException, NoSuchFieldException {
        return modify(entity, null);
    }

    @Override
    public int modify(CommonsEntity entity, Criteria criteria)
            throws DaoException, IllegalAccessException,
            InvocationTargetException, NoSuchMethodException,
            NoSuchFieldException {
        return DBUtil.getInstance(context).modify(context, entity, criteria,
                clss);
    }

    @Override
    public void modify(List<? extends CommonsEntity> entities)
            throws DaoException, IllegalAccessException,
            InvocationTargetException, NoSuchMethodException,
            NoSuchFieldException {
        if (!Collections.isListEmpty(entities)) {
            for (CommonsEntity entity : entities) {
                DBUtil.getInstance(context).modify(context, entity, null, clss);
            }
        }
    }

    @Override
    public void modifyGivenColumn(CommonsEntity entity,
                                  List<String> columnList, boolean isServerSideId)
            throws DaoException, IllegalAccessException,
            InvocationTargetException, NoSuchMethodException,
            NoSuchFieldException {
        // if (!Collections.isListEmpty(entities)) {
        // for (CommonsEntity entity : entities) {
        // DBUtil.getInstance(context).modify(context, entity, null, clss);
        // }
        // }
    }

    @Override
    public int delete(long id) throws DaoException, SQLException {
        return DBUtil.getInstance(context).delete(clss, context, null, id);
    }

    @Override
    public int delete(Criteria criteria) throws DaoException, SQLException {
        return DBUtil.getInstance(context).delete(clss, context, criteria, 0);
    }

    @Override
    public int delete(List<Long> ids) throws DaoException {
        return DBUtil.getInstance(context).delete(clss, context, ids);
    }

    @Override
    public int deleteAll() throws DaoException, SQLException {
        return DBUtil.getInstance(context).deleteAll(clss, context);
    }

    @Override
    public CommonsEntity selectById(long id)
            throws DaoException, SQLException, InstantiationException,
            IllegalAccessException, InvocationTargetException,
            NoSuchMethodException, NoSuchFieldException {
        return DBUtil.getInstance(context).select(context, clss, null, id);
    }

    @Override
    public List<? extends CommonsEntity> selectAll() throws DaoException,
            InstantiationException, IllegalAccessException,
            InvocationTargetException, NoSuchMethodException, SQLException,
            NoSuchFieldException {
        return select(CriteriaFactory.create(clss));
    }

    @Override
    public List<? extends CommonsEntity> select(Criteria criteria) throws DaoException, InstantiationException,
            IllegalAccessException, InvocationTargetException,
            NoSuchMethodException, SQLException, NoSuchFieldException {
        return DBUtil.getInstance(context).selectAll(context, clss, criteria,
                0);
    }

    @Override
    public List<? extends CommonsEntity> select(SQLiteQueryBuilder builder,
                                                List<String> columnNameList, String selection) throws DaoException,
            InstantiationException, IllegalAccessException,
            InvocationTargetException, NoSuchMethodException, SQLException,
            NoSuchFieldException {
        return DBUtil.getInstance(context).select(context, clss, builder,
                columnNameList, selection);
    }

    @Override
    public List<? extends Object> selectAllIds(Criteria criteria)
            throws DaoException, InstantiationException,
            IllegalAccessException, InvocationTargetException,
            NoSuchMethodException, SQLException, NoSuchFieldException {
        return DBUtil.getInstance(context).selectAllId(context, clss, criteria);
    }

    @Override
    public CommonsEntity selectOne(Criteria criteria) throws DaoException,
            InstantiationException, IllegalAccessException,
            InvocationTargetException, NoSuchMethodException, SQLException,
            NoSuchFieldException {
        return DBUtil.getInstance(context).select(context, clss, criteria, 0);
    }

    @Override
    public List<? extends CommonsEntity> rawQuery(String query) throws InstantiationException, IllegalAccessException, InvocationTargetException, NoSuchMethodException, NoSuchFieldException {
        List<String> columnNameList = CommonsEntity.getColumnNames(clss, false,
                false);
        Cursor cursor = DBUtil.getInstance(context).query(context, clss, query);
        List<CommonsEntity> commonsEntities = new ArrayList<CommonsEntity>();
        while (cursor.moveToNext()) {
            commonsEntities.add((CommonsEntity) DBUtil.getInstance(context).setValueToObject(cursor,
                    columnNameList, clss));
        }

        return commonsEntities;
    }

    public List<String> getStringListByRawQuery(String query,Object object) throws InstantiationException, IllegalAccessException, InvocationTargetException, NoSuchMethodException, NoSuchFieldException {
        List<String> columnNameList = CommonsEntity.getColumnNames(clss, false,
                false);
        Cursor cursor = DBUtil.getInstance(context).query(context, clss, query);
        List<String> commonsEntities = new ArrayList<>();
        while (cursor.moveToNext()) {
            commonsEntities.add((String) DBUtil.getInstance(context).setValueToObject(cursor,
                    columnNameList, clss));
        }

        return commonsEntities;
    }

    @Override
    public Cursor getCursor(String query) throws InstantiationException, IllegalAccessException, InvocationTargetException, NoSuchMethodException, NoSuchFieldException {
        return DBUtil.getInstance(context).query(context, clss, query);
    }


}
