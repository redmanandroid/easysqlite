package in.eightfolds.easysqlite.commons.db.exception;

public class DaoException extends Exception {
	private static final long serialVersionUID = 1L;

	public DaoException(String message){
		super(message);
	}
}
