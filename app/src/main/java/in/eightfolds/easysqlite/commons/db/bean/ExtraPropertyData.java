package in.eightfolds.easysqlite.commons.db.bean;

public class ExtraPropertyData {
	private String property;
	private Object value;

	public ExtraPropertyData(String property, String value) {
		this.property = property;
		this.value = value;
	}

	public String getProperty() {
		return property;
	}

	public void setProperty(String property) {
		this.property = property;
	}

	public Object getValue() {
		return value;
	}

	public void setValue(Object value) {
		this.value = value;
	}
}
