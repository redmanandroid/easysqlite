package in.eightfolds.easysqlite.commons.db.criteria;

import java.util.ArrayList;
import java.util.List;


public class CriteriaImpl implements Criteria {
	private Class<?> entity;
//	private List<String> columns;
	private List<Criterion> criterions;
	private List<String> groupBys;
	private List<Order> orderBys;
	private Integer startPosition;
	private Integer maxResults;
	
	protected CriteriaImpl(Class<?> entity){
		this.entity = entity;
//		this.columns = new ArrayList<String>();
		this.criterions = new ArrayList<Criterion>();
		this.groupBys = new ArrayList<String>();
		this.orderBys = new ArrayList<Order>();
	}

	@Override
	public Class<?> getEntity() {
		return entity;
	}
	
//	@Override
//	public List<String> getColumns() {
//		return columns;
//	}

	@Override
	public List<Criterion> getCriterions() {
		return criterions;
	}
	
	@Override
	public List<String> getGroupBys() {
		return groupBys;
	}

	@Override
	public List<Order> getOrderBys() {
		return orderBys;
	}
	
	@Override
	public Integer getFirstResult() {
		return startPosition;
	}

	@Override
	public Integer getMaxResults() {
		return maxResults;
	}
	
	

	
	@Override
	public Criteria add(Criterion criterion) {
		if(criterion != null){
			this.criterions.add(criterion);
		}
		return this;
	}
	
//	@Override
//	public Criteria addColumn(String column) {
//		if(column != null){
//			this.columns.add(column);
//		}
//		return this;
//	}
	
	@Override
	public Criteria addGroupBy(String propertyName) {
		if(propertyName != null){
			this.groupBys.add(propertyName);
		}
		return this;
	}

	@Override
	public Criteria addOrderBy(Order order) {
		if(order != null){
			this.orderBys.add(order);
		}
		return this;
	}
	
	@Override
	public Criteria setFirstResult(int startPosition) {
		this.startPosition = startPosition;
		return this;
	}

	@Override
	public Criteria setMaxResults(int maxResults) {
		this.maxResults = maxResults;
		return this;
	}

}
