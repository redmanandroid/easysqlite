package in.eightfolds.easysqlite.commons.util;

import java.util.List;

public class Collections {

	/**
	 * 
	 * @param list
	 *            list to validate
	 * @return false if the list is not null and not empty.
	 */
	public static boolean isListEmpty(List<?> list) {
		if (list != null && !list.isEmpty()) {
			return false;
		}
		return true;
	}

	public static String getCommaSeperatedStringFromArray(String[] ids) {
		if (ids == null || ids.length < 1) {
			return "";
		}
		StringBuilder rString = new StringBuilder();
		String sep = ",";
		for (String empId : ids) {
			rString.append(sep).append(empId);
		}
		return rString.toString().substring(1);
	}

}
