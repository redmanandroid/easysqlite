package in.eightfolds.easysqlite.commons.db.criteria;

public class LogicalExpression implements Criterion {
	public static final String AND = "and";
	public static final String OR = "or";
	
	private Criterion lhs;
	private Criterion rhs;
	private String op;
	
	protected LogicalExpression(Criterion lhs, Criterion rhs, String op) {
		this.lhs = lhs;
		this.rhs = rhs;
		this.op = op;
	}

	public Criterion getLhs() {
		return lhs;
	}

	public Criterion getRhs() {
		return rhs;
	}

	public String getOp() {
		return op;
	}

}
