package in.eightfolds.easysqlite.commons.db.bean;


import com.fasterxml.jackson.annotation.JsonIgnore;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

import in.eightfolds.easysqlite.commons.db.annotation.Column;
import in.eightfolds.easysqlite.commons.db.annotation.Entity;
import in.eightfolds.easysqlite.commons.db.annotation.Id;
import in.eightfolds.easysqlite.commons.db.annotation.Transient;
import in.eightfolds.easysqlite.commons.db.annotation.TransientOnUpdate;
import in.eightfolds.easysqlite.commons.util.Reflection;
import in.eightfolds.easysqlite.commons.util.Strings;


public class CommonsEntity {

	@JsonIgnore
	public static void getColumnNames(List<String> columnNameList,
			Class<?> clss, boolean skipId, boolean skipTransientOnUpdate) {
		if (clss.getSuperclass() != null && clss != CommonsEntity.class) {
			getColumnNames(columnNameList, clss.getSuperclass(), skipId,
					skipTransientOnUpdate);
		}

		Field[] fields = clss.getDeclaredFields();
		for (Field field : fields) {
			if (field.isAnnotationPresent(Transient.class)) {
				continue;
			}

			if (skipId && field.isAnnotationPresent(Id.class)) {
				continue;
			}

			if (skipTransientOnUpdate
					&& field.isAnnotationPresent(TransientOnUpdate.class)) {
				continue;
			}

			String columnName = field.getName();
			if (field.isAnnotationPresent(Column.class)) {
				columnName = field.getAnnotation(Column.class).name();
			}

			columnNameList.add(columnName);
		}

	}

	@JsonIgnore
	public static List<String> getColumnNames(Class<?> clss, boolean skipId,
			boolean skipTransientOnUpdate) {
		List<String> columnNameList = new ArrayList<String>();
		getColumnNames(columnNameList, clss, skipId, skipTransientOnUpdate);
		return columnNameList;
	}

	@JsonIgnore
	public List<String> getColumnNames(boolean skipId,
			boolean skipTransientOnUpdate) {
		return getColumnNames(this.getClass(), skipId, skipTransientOnUpdate);
	}

	@JsonIgnore
	public static String getEntityName(Class<?> clss) {
		String entityName = clss.getName();
		if (clss.isAnnotationPresent(Entity.class)) {
			entityName = clss.getAnnotation(Entity.class).name();
		}

		return entityName;
	}

	@JsonIgnore
	public String getEntityName() {
		return getEntityName(this.getClass());
	}

	@JsonIgnore
	public static String getAnnotatedColumnName(Class<?> clss,
			Class<? extends Annotation> annotationClass) {
		do {
			Field[] fields = clss.getDeclaredFields();
			for (Field field : fields) {
				if (field.isAnnotationPresent(Transient.class)) {
					continue;
				}

				if (field.isAnnotationPresent(annotationClass)) {
					String columnName = field.getName();
					if (field.isAnnotationPresent(Column.class)) {
						columnName = field.getAnnotation(Column.class).name();
					}

					return columnName;
				}
			}
		} while ((clss = clss.getSuperclass()) != null
				&& clss != CommonsEntity.class);

		return null;
	}

	@JsonIgnore
	public String getAnnotatedColumnName(
			Class<? extends Annotation> annotationClass) {
		return getAnnotatedColumnName(this.getClass(), annotationClass);
	}

	@JsonIgnore
	public static Class<?> getAnnotatedColumnType(Class<?> clss,
			Class<? extends Annotation> annotationClass) {
		Field[] fields = clss.getDeclaredFields();
		for (Field field : fields) {
			if (field.isAnnotationPresent(Transient.class)) {
				continue;
			}

			if (field.isAnnotationPresent(annotationClass)) {
				return field.getType();
			}
		}

		return null;
	}

	@JsonIgnore
	public Class<?> getAnnotatedColumnType(
			Class<? extends Annotation> annotationClass) {
		return getAnnotatedColumnType(this.getClass(), annotationClass);
	}

	@JsonIgnore
	public static boolean isAnnotatedColumnPresent(Class<?> clss,
			Class<? extends Annotation> annotationClass) {
		return clss.isAnnotationPresent(annotationClass);
	}

	@JsonIgnore
	public boolean isAnnotatedColumnPresent(
			Class<? extends Annotation> annotationClass) {
		return isAnnotatedColumnPresent(this.getClass(), annotationClass);
	}

	@JsonIgnore
	public static String getIdColumnName(Class<?> clss) {
		return getAnnotatedColumnName(clss, Id.class);
	}

	@JsonIgnore
	public String getIdColumnName() {
		return getIdColumnName(this.getClass());
	}

	@JsonIgnore
	public static boolean isIdColumnPresent(Class<?> clss) {
		return !Strings.isEmpty(getIdColumnName(clss));
	}

	@JsonIgnore
	public boolean isIdColumnPresent() {
		return isIdColumnPresent(this.getClass());
	}

	@JsonIgnore
	public static boolean isColumnExist(Class<?> clss, String columnName) {
		List<String> columnNames = getColumnNames(clss, false, false);
		if (columnNames == null) {
			return false;
		} else {
			return columnNames.contains(columnName);
		}
	}

	@JsonIgnore
	public boolean isColumnExist(String columnName) {
		return isColumnExist(this.getClass(), columnName);
	}

	// @JsonIgnore
	public long getEntityId() throws IllegalAccessException,
			InvocationTargetException, NoSuchMethodException,
			NoSuchFieldException {
		Object object = Reflection.getProperty(this, getIdColumnName());
		Long entityId = null;
		if (object instanceof Long) {
			entityId = (Long) object;
		} else if (object instanceof String) {
			try {
				entityId = Long.parseLong(object.toString());
			} catch (NumberFormatException e) {
				e.printStackTrace();
			}
		}

		if (entityId != null) {
			return entityId;
		}
		return 0;
	}

	// @JsonIgnore
	public void setEntityId(long id) throws IllegalAccessException,
			InvocationTargetException, NoSuchMethodException,
			NoSuchFieldException {
		Reflection.setProperty(this, getIdColumnName(), id);
	}

}
