package in.eightfolds.easysqlite.commons.util;

import java.util.List;
import java.util.Random;

public class Strings {
	public static String toCSV(String[] strings) {
		if (strings != null) {
			String csv = "";
			for (String string : strings) {
				if (csv.length() > 0) {
					csv += ", ";
				}

				csv += string;
			}

			return csv;
		} else {
			return null;
		}
	}

	public static String toCSV(List<?> list) {
		if (list != null) {
			String csv = "";
			for (Object obj : list) {
				if (csv.length() > 0) {
					csv += ", ";
				}

				csv += obj.toString();
			}

			return csv;
		} else {
			return null;
		}
	}

	public static String toString(String[] strings, String seperator) {
		if (strings != null) {
			String str = "";
			for (String string : strings) {
				if (str.length() > 0) {
					str += seperator;
				}

				str += string;
			}

			return str;
		} else {
			return null;
		}
	}

	public static String getRandomString(int length) {
		String chars = "0123456789abcdefghijklmonpqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
		Random r = new Random();

		char[] buf = new char[length];

		for (int i = 0; i < buf.length; i++) {
			buf[i] = chars.charAt(r.nextInt(chars.length()));
		}

		return new String(buf);
	}
	
	public static boolean startsWith(String str, String prefix,
			boolean ignoreCase) {
		if (str == null || prefix == null) {
			return (str == null && prefix == null);
		}
		if (prefix.length() > str.length()) {
			return false;
		}
		return str.regionMatches(ignoreCase, 0, prefix, 0, prefix.length());
	}

	public static String makeNullIfEmpty(String value) {
		if (value != null && value.trim().length() == 0) {
			return null;
		} else {
			return value;
		}
	}

	public static String makeEmptyIfNull(String value) {
		if (value == null) {
			return "";
		} else {
			return value;
		}
	}
	
	public static boolean isEmpty(String string1) {
		if (string1 != null && string1.length() > 0) {
			return false;
		} else {
			return true;
		}
	}

	public static boolean isEqual(String string1, String string2) {
		if (string1 == null) {
			if (string2 == null) {
				return true;
			} else {
				return false;
			}
		} else {
			return string1.equals(string2);
		}
	}

	public static boolean isEqualIgnoreCase(String string1, String string2) {
		if (string1 == null) {
			if (string2 == null) {
				return true;
			} else {
				return false;
			}
		} else {
			return string1.equalsIgnoreCase(string2);
		}
	}

	public static String replaceAllNewLineChar(String input) {
		String output = replaceAllSkipNull(input, "\n", " ");
		output = replaceAllSkipNull(output, "\r", " ");
		return output;
	}

	public static String replaceAllSkipNull(String input, String regex,
			String replacement) {
		if (input != null) {
			return input.replaceAll(regex, replacement);
		} else {
			return null;
		}
	}
}
