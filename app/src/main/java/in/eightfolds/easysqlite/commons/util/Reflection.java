package in.eightfolds.easysqlite.commons.util;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;

import in.eightfolds.easysqlite.commons.db.bean.CommonsEntity;


public class Reflection {

	public static Object getNewInstance(Class<?> clazz)
			throws InstantiationException, IllegalAccessException {
		return clazz.newInstance();
	}

	public static void setProperty(Object object, String property, Object value)
			throws IllegalAccessException, InvocationTargetException,
			NoSuchMethodException, NoSuchFieldException {
		Class<?> clazz = object.getClass();
		do {
			for (Field field : clazz.getDeclaredFields()) {
				if (field.getName().equalsIgnoreCase(property)) {
					field.setAccessible(true);
					field.set(object, value);
					return;
				}
			}
		} while ((clazz = clazz.getSuperclass()) != null
				&& clazz != CommonsEntity.class);
	}

	public static void setPropertyIfExists(Object object, String property,
			Object value) throws IllegalAccessException,
			InvocationTargetException, NoSuchMethodException,
			NoSuchFieldException {

		Class<?> clazz = object.getClass();
		do {
			for (Field field : clazz.getDeclaredFields()) {
				if (field.getName().equalsIgnoreCase(property)) {
					field.setAccessible(true);
					field.set(object, value);
					return;
				}
			}
		} while ((clazz = clazz.getSuperclass()) != null
				&& clazz != CommonsEntity.class);
	}

	public static Object getProperty(Object object, String property)
			throws IllegalAccessException, InvocationTargetException,
			NoSuchMethodException, NoSuchFieldException {
		Class<?> clazz = object.getClass();

		do {
			for (Field field : clazz.getDeclaredFields()) {
				if (field.getName().equalsIgnoreCase(property)) {
					field.setAccessible(true);
					return field.get(object) != null ? field.get(object)
							.toString() : null;
				}
			}
		} while ((clazz = clazz.getSuperclass()) != null
				&& clazz != CommonsEntity.class);

		return null;

	}

	public static Class<?> getPropertyType(Object object, String property)
			throws IllegalAccessException, InvocationTargetException,
			NoSuchMethodException, NoSuchFieldException {
		Class<?> clazz = object.getClass();
		do {
			try {
				Field field = clazz.getDeclaredField(property);
				return field.getType();
			} catch (NoSuchFieldException e) {
				e.printStackTrace();
			}
		} while ((clazz = clazz.getSuperclass()) != null
				&& clazz != CommonsEntity.class);
		return null;
	}

}
