package in.eightfolds.easysqlite.commons.db.criteria;

public class CriteriaFactory {
	
	public static Criteria create(Class<?> entity){
		return new CriteriaImpl(entity);
	}

}
