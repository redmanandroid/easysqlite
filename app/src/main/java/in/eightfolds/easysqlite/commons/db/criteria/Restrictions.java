package in.eightfolds.easysqlite.commons.db.criteria;

import java.util.Collection;

public class Restrictions {
	public static LogicalExpression and(Criterion lhs, Criterion rhs){
		return new LogicalExpression(lhs, rhs, LogicalExpression.AND);
	}
	public static LogicalExpression or(Criterion lhs, Criterion rhs){
		return new LogicalExpression(lhs, rhs, LogicalExpression.OR);
	}
	public static PropertyExpression between(String propertyName, Object lo, Object hi) {
		return new PropertyExpression(propertyName, lo, hi, PropertyExpression.BETWEEN);
	}
	public static PropertyExpression idEq(Object value) {
		return new PropertyExpression(null, value, PropertyExpression.EQUAL);
	}
	public static PropertyExpression eq(String propertyName, Object value) {
		return new PropertyExpression(propertyName, value, PropertyExpression.EQUAL);
	}
	public static PropertyExpression eqProperty(String propertyName, String otherPropertyName)  {
		return new PropertyExpression(propertyName, otherPropertyName, PropertyExpression.EQUAL);
	}
	public static PropertyExpression ne(String propertyName, Object value) {
		return new PropertyExpression(propertyName, value, PropertyExpression.NOT_EQUAL);
	}
	public static PropertyExpression neProperty(String propertyName, String otherPropertyName) {
		return new PropertyExpression(propertyName, otherPropertyName, PropertyExpression.NOT_EQUAL);
	}
	public static PropertyExpression isNull(String propertyName) {
		return new PropertyExpression(propertyName, PropertyExpression.IS_NULL);
	}
	public static PropertyExpression isNotNull(String propertyName)  {
		return new PropertyExpression(propertyName, PropertyExpression.IS_NOT_NULL);
	}
	public static PropertyExpression ge(String propertyName, Object value) {
		return new PropertyExpression(propertyName, value, PropertyExpression.GE);
	}
	public static PropertyExpression geProperty(String propertyName, String otherPropertyName)  {
		return new PropertyExpression(propertyName, otherPropertyName, PropertyExpression.GE);
	}
	public static PropertyExpression gt(String propertyName, Object value) {
		return new PropertyExpression(propertyName, value, PropertyExpression.GT);
	}
	public static PropertyExpression gtProperty(String propertyName, String otherPropertyName)  {
		return new PropertyExpression(propertyName, otherPropertyName, PropertyExpression.GT);
	}
	public static PropertyExpression le(String propertyName, Object value) {
		return new PropertyExpression(propertyName, value, PropertyExpression.LE);
	}
	public static PropertyExpression leProperty(String propertyName, String otherPropertyName)  {
		return new PropertyExpression(propertyName, otherPropertyName, PropertyExpression.LE);
	}
	public static PropertyExpression lt(String propertyName, Object value) {
		return new PropertyExpression(propertyName, value, PropertyExpression.LT);
	}
	public static PropertyExpression ltProperty(String propertyName, String otherPropertyName) {
		return new PropertyExpression(propertyName, otherPropertyName, PropertyExpression.LT);
	}
	public static PropertyExpression like(String propertyName, Object value) {
		return new PropertyExpression(propertyName, value, PropertyExpression.LIKE);
	}
	public static PropertyExpression in(String propertyName, Collection<?> values) {
		return new PropertyExpression(propertyName, values, PropertyExpression.IN);
	}
	public static PropertyExpression in(String propertyName, Object[] values) {
		return new PropertyExpression(propertyName, values, PropertyExpression.IN);
	}
	public static Junction conjunction(){
		return new Junction(Junction.CONJUNCTION);
	} 
	public static Junction disjunction(){
		return new Junction(Junction.DISJUNCTION);
	}
}
